```
Aplomb Scribe
Martin's Liberty Basic Pre-compiler
by Martin "Hooshnik" Truesdell
parses "include" files in source file
and other fine things
Dedicated to Aaron Swartz
```

#License:

-------------------------------------------------
Creative Commons Attribution (BY) license:
Licensees may copy, distribute, display and perform the work and make derivative works and remixes based on it only if they give the author or licensor the credits (attribution) in the manner specified by these.
-------------------------------------------------

#Warnings:

WARNING!  I don't recommend using the Liberty Basic IDE as your IDE with aplombscribe if and at all possible. AS adds the prefix "DONOTEDIT" so you know not to edit the processed file which is then launched by Liberty Basic. If you edit the DONOTEDIT file your changes will (naturally) be lost. I personally use vim but you can use geany or any other IDE. Vim and Geany both provide a way of automatically launching AS after setting it up properly but pc.bat can also be used (check the 'sample' directory).

##MICROSOFT WINDOWS RELATED PROBLEMS:


-Windows 7 (even windows 8.1 with better SMB is affected?) and lower do not use SMB 3.0 or higher. If you are running goatscribe on a network drive on those older OS beware. Sometimes changes to files are not communicated properly before 10 seconds. If goatscribe runs it can read old changes of files and get confused. You will need to reset the project by deleting the goattimes.raf file. 

UPDATE:  You can now set an environmental variable. Goatscribe gives you an info message on startup describing it. Goatscribe can be made to count down from 10 second mark before execution.

This DOES NOT fix it for me on windows 7:

https://stackoverflow.com/questions/5159220/windows-file-share-why-sometimes-newly-created-files-arent-visible-for-some-pe

quote
The default value for this wonderful little cache is 10 seconds, which is producing the behavior you're seeing. When your code asks the system about that directory/file it's getting the cached result, which is 10 seconds old, so it says the file does not exist. Setting HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Lanmanworkstation\Parameters\DirectoryCacheLifetime (DWORD) to a value of 0 will disable the cache and resolve the file doesn't exist issue. Surprisingly this change does not require a restart of the client machine! This will also allow you to keep SMB2 enabled, which should be better for a number of reasons vs. forcing SMB1.
endquote

The only possible fix is to wait more than 10 seconds after writing a file when you want to compile  or else you have no choice but to delete the goattimes.raf file (or don't run GS from a SMB drive). 


#Docs:

I apologize if my documentation is not clear.

Welcome to Aplomb Scribe (AS). With it you can do a few things in a pre-processor way for your Liberty Basic programs. It has no GUI except the use of the mainwin. Its main feature is the ability to add include files to your program. This program does not support real C++ style include strength and these "include" files are really just macros. What I mean is you can't "include once" - AS doesn't have checks to see if something has already been included. You can include the same file again somewhere else. It supports a file depth of 2. I was going to add infinite depth but I saw LB has a problem with recursion in functions (supposedly fixed in v4.5.1 but not written in changelogs). So if I have a macro in main.bas and then another macro in that macro that is as far as I can go.

AS will insert the file exactly at the point specified so if you have only functions and subs in it you will want to put it at the end of the file like this:

```
<footermath.bas>
<footerapitools.bas>
```

Right now there is no command in LB that starts with an angled bracket so trying to compile without AS gives an error. If you really want something that appends the files to the end of the main file you can use Liberty Basic Workshop's include system but be warned that is is no longer maintained by the author and is not open source. It has been reported to have problems running on some operating systems for some users (which is one of the reasons why I made AS).

There are other uses for macros, something called 'inline functions'. LB doesn't have them. One unintended side effect is you can organize code that has been indented too much. Normally if you are indenting too many times it's like a warning that you should redesign your code. However if you want to use the cpu less and the RAM more you could put a section of code inline sort of like this:

```
	if blah then
		if moreblah then
			for i = 1 to foo
				<myloopstuff.bas>
			next
		end if
	end if
```

That way by putting the code into myloopstuff.bas you avoid having too much indentation.

##Syntax for this program:

Here is the syntax for the program if you run it without parameters using the console:
```
aplombscribe -a -b -s -t [-d | -r] 'bodyfile.bas' 'c:\path_to_liberty_basic\liberty.exe'
```
###Example to run program on 64 bit system:
aplombscribe -a -r 'myprogram.bas' 'c:\Program Files (x86)\Liberty BASIC v4.04\liberty.exe'

###Switches:
```
Liberty Basic specific compiler switches:
-a : close all windows on exit
-d : debug
-r : run

this program's switches:
-b : build mode, don't launch liberty basic just make DONOTEDIT file
-s : enable user defined strict warnings (stop on warning)
-t : turn on test mode (don't write changes to disk, don't launch anything)
-e : turn on expert mode (do not ask me for support for this switch)
-v : print program version
```

strict mode can be slow, using it with test mode recommended

NOW! Normally you do not run the exe directly. You want to run it from a batch file (or using a function key from Geany for example) so I have provided pc.bat. The typical line to launch AS would be:
```
	pc myprogrambody.bas -r
```
or run it with debug:
```
	pc myprogram.bas -d
```
If it sees the word "body" in the filename it will remove it in the DONOTEDIT file it creates (currently goatscribe doesn't do this as of 2018-10-08 and there is no plans to reintroduce it). Normally I append the word 'body' to my main file. Please edit pc.bat for your needs. You will need to know if you have a 64 bit "Program Files (x86)"  or 32 bit "Program Files" windows and whether or not you use LB Pro "Liberty BASIC Pro vX.XX" or Standard "Liberty BASIC vX.XX". Change the line if needed:
```
	aplombscribe -a %2 %3 %4 %5 %6 %7 %8 %9 '%1' 'c:\Program Files (x86)\Liberty BASIC Pro v4.5.1\lbpro.exe'
```
You will notice that the '-a' switch is on by default. It ensures that LB closes with less clicking.

If you have lots of macros and find the standard file existance checks are taking too long try using the expert switch. AS will crash with an ambiguous error message if you have a missing file using the expert switch so watch out.

Well that's all you need to know to get started so don't use strict mode for now. If you want to use strict mode look at the expert docs below (strict mode has been removed in goatscribe and there are currently (2018-10-08) no plans to put it back in. If this happened it would probably come back under perl REGEX patterns).

#STRICT WARNINGS DOCS:

___This is like regex but less powerful. Seriously. This could $(^@#%()^%)(#) up in your face.___

warningcheck.cfg is needed for strict mode. I have provided a sample of this file. I will try to explain some lines with tilde '~' as the delimeter. Also see the file itself for more docs (in fact read it first then come back here). Matches are from the beginning of the line and takes indentation into account.

standard comment line:
```
	<c>~WRITE EVERYTHING IN LOWER CASE EXCEPT FOR THE ERROR MESSAGE
```
if gosub is seen give error 'use of gosub depricated':
```
	<w>~gosub~use of gosub depricated~~
```
Will do nothing because it is a commented line:
```
	<c>~<w>~call convertir~(test) convertir detected with 'for' before~for~
```
look for 2 lines like this:  "for" blah blah blah could be anything here. then on the next line "call convertir" then trigger the error message "(test) convertir detected with 'for' before'". If it had only found the second line it would not have triggered the error message:
```
	<w>~call convertir~(test) convertir detected with 'for' before~for~
```
Here is a great one! If a line starts with '#' instead of the usual 'print #' then look at the previous line. If the previous line DOES NOT CONTAIN 'call convertir' then print the error 'possible print without conversion'. You can put an exclamaion point in front to mean not for the before and after fields. The last 2 arguments are the exceptions for the current line and there can be as many as the array can hold (current limit looks like 1000). So if it sees 'mainWindow' or 'configuration' it will abort showing the error message.
```
	<w>~#~possible print without conversion~!call convertir~~mainWindow~configuration
```
Exactly the same as the last one except it will look for NOT 'call convertir' AFTER the current line instead of before. Examine the tildes closely!

```
	<w>~#~possible print without conversion~~!call convertir~mainWindow~configuration
```

#Bugs:

* attempting to go beyond a file depth of 2 will result in a syntax error in the LB compiler as it will see a line beginning with an angled bracket. I designed it this way partly to get more speed.
