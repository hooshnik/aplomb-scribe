' encoded in winansi windows-1252 marker -> (�)
''''''''''''''''''''''''''''''''''''''''''''
' Aplomb Scribe
' Martin's Liberty Basic Pre-compiler
' by Martin "Hooshnik" Truesdell
' Created 2013-08-08
'
' parses "include" files in source file
' and other fine things
''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Creative Commons Attribution (BY) license:
'Licensees may copy, distribute, display and perform the work and make
'derivative works and remixes based on it only if they give the author
'or licensor the credits (attribution) in the manner specified by these.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


' for help run aplombscribe without any parameters

' TODO:  make macros opened more than once not check for existance again
' for more speed (alternative:	use expert mode switch which is in
' the program now)

' PROGRAM VERSION HERE
programVersion$ = "1.3.0"

beginningOfSwitchesPos = 0
beginningOfFilePos = 0
charPos = 0
firstSpace = 0
bodyFoundFlag = 0
maxWarnItems = 1000
warnListCount = 0
parentLineCounter = 1

libertyBasicExecutable$ = ""

entireCommandLine$ = CommandLine$

dim info$(10,10)
dim warningCheckTable$(maxWarnItems, maxWarnItems)
dim exceptions(maxWarnItems)

' debug lines
'entireCommandLine$ = "aplombscribe -a -s -t 'file1body.bas' 'c:\Program Files (x86)\Liberty BASIC v4.04\liberty.exe'"
'entireCommandLine$ = "aplombscribe 'file1body.bas' 'c:\Program Files (x86)\Liberty BASIC v4.04\liberty.exe'"
'entireCommandLine$ = "aplombscribe -r 'testmain.bas' 'c:\Program Files (x86)\Liberty BASIC v4.04\liberty.exe'"

if word$(entireCommandLine$, 2) = "" then
	call showHelp
	call showVersion programVersion$
	call enterExits
	end
end if

print "body path = " + DefaultDir$
print "command line = " + entireCommandLine$

do
	charPos = charPos + 1
	singleChar$ = mid$(entireCommandLine$, charPos, 1)
loop while validateFilenameChar(singleChar$, 0)

firstSpace = charPos

for charPos = firstSpace to len(entireCommandLine$)
	singleChar$ = mid$(entireCommandLine$, charPos, 1)
	select case
	case singleChar$ = chr$(32)
		' do nothing

	case singleChar$ = chr$(45)
		' ascii 45 is a minus
		beginningOfSwitchesPos = charPos
		do
			charPos = charPos + 1
			singleChar$ = mid$(entireCommandLine$, charPos, 1)
		loop while validateSwitchChar(singleChar$)

		oneSwitch$ = mid$(entireCommandLine$, _
		beginningOfSwitchesPos, charPos + 1 - beginningOfSwitchesPos)

		select case
		case trim$(oneSwitch$) = "-v"
				print "Program Version v" ; programVersion$
				call enterExits
				end
				
		case trim$(oneSwitch$) = "-b"
			' turn on build mode (don't launch liberty basic but write
			' changes to disk)
			buildMode = 1
			print "build mode activated"

		case trim$(oneSwitch$) = "-s"
			' turn on warnings and do not compile on warning for safety
			strictWarnings = 1
			print "strict warnings mode activated"

		case trim$(oneSwitch$) = "-t"
			' turn on test mode (don't write changes to disk,
			' don't launch anything)
			testMode = 1
			print "test mode activated"

		case trim$(oneSwitch$) = "-e"
			' turn on expert mode (crash hard when file does not exist
			' (for more speed))
			expertMode = 1
			print "expert mode activated"

		case else
			commandLineSwitches$ = commandLineSwitches$ + oneSwitch$
			print "found non-AS switch:  " ; oneSwitch$
		end select

		'debug line
		'print "commandLineSwitches so far = " + commandLineSwitches$
		' now 1 switch has been added to total switches

	case validateFilenameChar(singleChar$, 1)
		' alphanumeric filename, single quotes expected around it
		beginningOfFilePos = charPos
		do
			charPos = charPos + 1
			singleChar$ = mid$(entireCommandLine$, charPos, 1)
		loop while validateFilenameChar(singleChar$, 1)

		if bodyFoundFlag = 0 then
			bodyFoundFlag = 1
			bodyName$ = mid$(entireCommandLine$, beginningOfFilePos, charPos - beginningOfFilePos)

			print "bodyName = " + bodyName$
			if fileExists(DefaultDir$ + "\" + bodyName$) = 0 then	  '"
				print "error body file does not exist (it should be enclosed in quotes)"
				call enterExits
				end
			end if
		else
			libertyBasicExecutable$ = mid$(entireCommandLine$, beginningOfFilePos, charPos - beginningOfFilePos)
			print "libertyBasicExecutable = " + libertyBasicExecutable$
			if fileExists(libertyBasicExecutable$) = 0 then
				print "error liberty basic executable not found at end of command line or at specified location (it should be enclosed in quotes)"
				call enterExits
				end
			end if
		end if
	end select

next

select case
case beginningOfSwitchesPos = 0
	' no switches is bad
	print ""
	print "ERROR:  need at least one switch!"
	print ""
	call showHelp
	call enterExits
	end
case bodyName$ = ""
	' no file is bad
	print ""
	print "ERROR:  need a filename!"
	print ""
	call showHelp
	call enterExits
	end
end select

' get outputName from body name
outputName$ = bodyName$

' remove "body" from the end of the filename if it's there
if right$(bodyName$, 8) = "body.bas" then
	outputName$ = left$(bodyName$, len(bodyName$) - 8) + ".bas"
end if

' add DONOTEDIT to remind the programmer never to edit this file
dneOutputName$ = "DONOTEDIT" + outputName$
print "output name with do not edit prefix = " + dneOutputName$

outputName$ = dneOutputName$

if not(expertMode) then
	' first check to see if all header are existing first
	open bodyName$ for input as #body

	while not(eof(#body))
		line input #body, fileio$
		headerName$ = detectMetaLine$(fileio$)
		if headerName$ <> "" and headerName$ <> "'" then
			if not(fileExists(DefaultDir$ + "\" + headerName$)) then '"
				close #body
				print "error <" + headerName$ + "> does not exist"
				call enterExits
				end
			else
				' it does exist. open it and check for macros
				open headerName$ for input as #header
				while not(eof(#header))
					line input #header, fileio$
					macroName$ = detectMetaLine$(fileio$)
					if macroName$ <> "" and macroName$ <> "'" then
						if not(fileExists(DefaultDir$ + "\" + macroName$)) then '"
							close #header
							close #body
							print "error <" + macroName$ + "> does not exist"
							call enterExits
							end
						end if
					end if
				wend
				close #header

			end if
		end if
	wend

	close #body
end if

if strictWarnings = 1 then
	configExistence = loadConfig(warnListCount, maxWarnItems)
	if configExistence = 0 then
		print "config file for strict warnings does not exist!"
		call enterExits
		end
	end if

end if

open bodyName$ for input as #body

if not(testMode) then
	open outputName$ for output as #complete
end if

print "opened body and output files successfully"

while not(eof(#body))
	line input #body, fileio$

	headerName$ = detectMetaLine$(fileio$)
	if headerName$ <> "" and headerName$ <> "'" then
		call copyHeader warningCounter, warnListCount, headerLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, headerName$, testMode, strictWarnings

	else
		if not(testMode) then
			print #complete, fileio$
		end if
		if strictWarnings = 1 then
			call strictWarningProcessor warningCounter, warnListCount, headerLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, "", fileio$
			if eof(#body) then
				' call one more time with a blank for the end of the file
				fileio$ = ""
				call strictWarningProcessor warningCounter, warnListCount, headerLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, "", fileio$
			end if
		end if

	end if
wend

if not(testMode) then
	close #complete
end if

close #body

if warningCounter > 0 and strictWarnings = 1 then
	print "compile aborted due to " ; str$(warningCounter) ; " warnings!"
	' uncomment to run notepad to debug logfile (windows only of course)
	'run "notepad aplombscribe.log"
	call enterExits
	end
else
	print "0 warnings reported"
	if testMode then
		call enterExits
		end
	end if
end if

' wait some time for completed file to be written to disk
' needed for external compiler to see complete file
' TODO: add flush cache command like in goatscibe
call timeSleep 100

if buildMode = 0 then
	' if we get here we are in normal mode - launch liberty basic
	runLine$ = libertyBasicExecutable$ + " " + commandLineSwitches$ + " " + DefaultDir$ + "\" + outputName$   '"

	print runLine$
	run runLine$

end if

end


'*****************************************************
'************* debut les functions *******************
'*****************************************************

' subroutines are considered functions and go here in no particular order

sub enterExits

print
input "Hit ENTER to exit ..."; dummy

end sub


sub copyHeader byref warningCounter, byref warnListCount, byref headerLineCounter, byref parentLineCounter, byref currentLine$, byref previousLine$, byref nextLine$, headerName$, testMode, strictWarnings

open headerName$ for input as #header
print "opened " + headerName$ + " successfully"

while not(eof(#header))
	line input #header, fileio$

	macroName$ = detectMetaLine$(fileio$)
	if macroName$ <> "" and macroName$ <> "'" then
		call copyMacro warningCounter, warnListCount, macroLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, macroName$, testMode, strictWarnings

	else
		if not(testMode) then
			print #complete, fileio$
		end if
		if strictWarnings = 1 then
			call strictWarningProcessor warningCounter, warnListCount, headerLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, headerName$, fileio$
			if eof(#header) then
				' call one more time with a blank for the end of the file
				fileio$ = ""
				call strictWarningProcessor warningCounter, warnListCount, headerLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, headerName$, fileio$
			end if

		end if
	end if
wend

close #header

headerName$ = ""

if strictWarnings = 1 then
	headerLineCounter = 0
end if

end sub


sub copyMacro byref warningCounter, byref warnListCount, byref macroLineCounter, byref parentLineCounter, byref currentLine$, byref previousLine$, byref nextLine$, macroName$, testMode, strictWarnings
' the reason why this function/sub looks like copyHeader is because
' Liberty Basic has unreliable recursion in 4.04 and doing it this
' way prevents that bug. Rumor is it's fixed in v4.5.1.

open macroName$ for input as #macro
print "opened " + macroName$ + " successfully"

while not(eof(#macro))
	line input #macro, fileio$
	if not(testMode) then
		print #complete, fileio$
	end if
	if strictWarnings = 1 then
		call strictWarningProcessor warningCounter, warnListCount, macroLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, macroName$, fileio$
		if eof(#macro) then
			' call one more time with a blank for the end of the file
			fileio$ = ""
			call strictWarningProcessor warningCounter, warnListCount, macroLineCounter, parentLineCounter, currentLine$, previousLine$, nextLine$, macroName$, fileio$
		end if

	end if
wend

close #macro

macroName$ = ""

if strictWarnings = 1 then
	macroLineCounter = 0
end if

end sub


function loadConfig(byref warnListCount, maxWarnItems)
' depends on info$()
' first line is the user defined delimeter character
' valid lines start with a tag then a delimeter
' 1: the search pattern for the current line
' 2: the error message
' 3: what fragment has to be present
' ...on the previous line for a warning, if any
' 4: what fragment has to be present on the next line for a warning, if any
' ...if none for previous and next line put 2 delimiters in a row
' 5 and beyond: exceptions immediately after the search pattern
' ...for the current line. you can have as many as you want
' see sample warningcheck.cfg file

loadConfig = 0 ' default is bad condition
endOfTheFile = 0
endOfTheLine = 0
warnListCount = 0

file$ = "warningcheck.cfg"

files DefaultDir$, "\" + file$, info$()    '"
loadConfig = val(info$(0, 0))

' debug lines
print "reading from " + DefaultDir$ + "\" + file$	  '"
print "file exist condition = " + str$(fileexist)
print ""

if loadConfig > 0 then
	open file$ for input as #config
	input #config, fileio$
	if len(fileio$) = 1 then
		delimeter$ = fileio$
	else
		close #config
		print "delimeter in warningcheck.cfg is not 1 character!"
		call enterExits
		end
	end if

	fileio$ = inputto$(#config, delimeter$)
	
	' debugging logfile
	logHandle$ = "#logfile"
	open "aplombscribe.log" for output as #logHandle$

	while not(eof(#config))
		select case
		case fileio$ = "<w>"
			warnListCount = warnListCount + 1
			' if true then we are the beginning of a valid config line
			warningCheckTable$(warnListCount, 1) = inputto$(#config, delimeter$)
			' match to look for
			warningCheckTable$(warnListCount, 2) = inputto$(#config, delimeter$)
			' error message that goes with match
			warningCheckTable$(warnListCount, 3) = inputto$(#config, delimeter$)
			' what should be on previous line if any
			warningCheckTable$(warnListCount, 4) = inputto$(#config, delimeter$)
			' what should be on next line if any
			'
			if eof(#config) then
				exit while
			end if

			warnColumn = 5
			exceptions(warnListCount) = 0
			endOfTheLine = 0
			do
				fileio$ = inputto$(#config, delimeter$)
				if not(fileio$ = "<c>" or fileio$ = "<w>" or eof(#config)) then
					if exceptions(warnListCount) = maxWarnItems then
						close #config
						print "maximum exceptions of " ; str$(maxWarnItems) ; " exceeded, increase array size"
						call enterExits
						end
					end if
					if fileio$ = "" then
						close #config
						print "error:  blank exceptions not allowed"
						print "check warning called '" ; warningCheckTable$(warnListCount, 1) ; "'"
						call enterExits
						end
					end if

					fileio$ = lower$(fileio$)
					warningCheckTable$(warnListCount, warnColumn) = fileio$
					exceptions(warnListCount) = exceptions(warnListCount) + 1
					warnColumn = warnColumn + 1
				else
					endOfTheLine = 1
					exit do
				end if
				endOfTheFile = eof(#config)
			loop while not(endOfTheFile)

			if warnListCount = maxWarnItems then
				close #config
				print "maximum items of " ; str$(maxWarnItems) ; " exceeded, increase array size"
				call enterExits
				end
			end if

			' debugging logfile block begin '''''''''''''''
			print #logHandle$, ""
			print #logHandle$, "found warning type " ; str$(warnListCount)
			print #logHandle$, "match:  " ; warningCheckTable$(warnListCount, 1)
			print #logHandle$, "message:  " ; warningCheckTable$(warnListCount, 2)
			print #logHandle$, "needed before line:  " ; warningCheckTable$(warnListCount, 3)
			print #logHandle$, "needed after line:  " ; warningCheckTable$(warnListCount, 4)
			for i = 1 to exceptions(warnListCount)
				  print #logHandle$, "exception # "; str$(i) ; " is:  " ; warningCheckTable$(warnListCount, i + 4)
			next
			' debugging logfile block end '''''''''''''''''

			print "found warning type " ; str$(warnListCount)
			print "match:  " ; warningCheckTable$(warnListCount, 1)
			print "message:  " ; warningCheckTable$(warnListCount, 2)
			print "needed before line:	  " ; warningCheckTable$(warnListCount, 3)
			print "needed after line:  " ; warningCheckTable$(warnListCount, 4)
			for i = 1 to exceptions(warnListCount)
				print "exception # "; str$(i) ; " is:  " ; warningCheckTable$(warnListCount, i + 4)
			next
			print ""

		case fileio$ = "<c>"
			' comment detected, do nothing
			line input #config, dummy$
			if eof(#config) then
				exit while
			end if
			' read again
			fileio$ = inputto$(#config, delimeter$)

		case else
			close #config
			print "'<c>' or '<w>' expected on start of line but not found!"
			call enterExits
			end

		end select

	wend

	' debugging logfile
	close #logHandle$

	close #config

end if

end function


function detectMetaLine$(fileio$)
' detects metalines in source code like comments and "include" files

fileio$ = trim$(fileio$)

select case left$(fileio$, 1)
case "'"
	detectMetaLine$ = "'"

case "<"
	for charPos = 2 to len(fileio$)
		if mid$(fileio$, charPos, 1) = ">" then
			detectMetaLine$ = mid$(fileio$, 2, charPos - 2)
		end if
	next

end select

end function


function fileExists(fullPath$)

files pathOnly$(fullPath$), filenameOnly$(fullPath$), info$()
fileExists = val(info$(0, 0)) > 0

end function


function pathOnly$(fullPath$)

pathOnly$ = fullPath$

while right$(pathOnly$, 1) <> "\" and pathOnly$ <> ""	   ' "
	pathOnly$ = left$(pathOnly$, len(pathOnly$)-1)
wend


end function


function filenameOnly$(fullPath$)

pathLength = len(pathOnly$(fullPath$))
filenameOnly$ = right$(fullPath$, len(fullPath$)-pathLength)

end function


function validateSwitchChar(singleChar$)

a = asc(singleChar$)

' allowed characters are alphanumeric, period, colon, backslash
if (a >= 97 and a <= 122) _
	or (a >= 65 and a <= 90) _
	or (a >= 48 and a <= 57) _
	or a = 46 _
	or a = 58 _
	or a = 92 then
	validateSwitchChar = 1
else
	validateSwitchChar = 0
end if

end function


function validateFilenameChar(singleChar$, spacesAllowed)

a = asc(singleChar$)

' allowed characters are space (with spacesAllowed set),
' alphanumeric, period, colon, backslash, parenthesis, underscore, minus)
select case	spacesAllowed
case 0
	if (a >= 97 and a <= 122) _
		or (a >= 65 and a <= 90) _
		or (a >= 48 and a <= 57) _
		or a = 46 _
		or a = 58 _
		or a = 92 _
		or (a >= 40 and a <= 41) _
		or a = 95 _
		or a = 45 then
		validateFilenameChar = 1
	else
		validateFilenameChar = 0
	end if

case 1
	if a = 32 _
		or (a >= 97 and a <= 122) _
		or (a >= 65 and a <= 90) _
		or (a >= 48 and a <= 57) _
		or a = 46 _
		or a = 58 _
		or a = 92 _
		or (a >= 40 and a <= 41) _
		or a = 95 _
		or a = 45 then
		validateFilenameChar = 1
	else
		validateFilenameChar = 0
	end if

end select

end function


sub strictWarningProcessor byref warningCounter, byref warnListCount, byref headerLineCounter, byref parentLineCounter, byref currentLine$, byref previousLine$, byref nextLine$, headerName$, fileio$

previousLine$ = currentLine$
currentLine$ = nextLine$
nextLine$ = fileio$

isComment$ = detectMetaLine$(currentLine$)

if isComment$ <> "'" and headerName$ <> "" then
	call warningCheck warningCounter, warnListCount, headerLineCounter, headerName$, currentLine$, previousLine$, nextLine$
end if

if isComment$ <> "'" and headerName$ = "" then
	call warningCheck warningCounter, warnListCount, parentLineCounter, "", currentLine$, previousLine$, nextLine$
end if

if headerName$ = "" then
	parentLineCounter = parentLineCounter + 1
else
	headerLineCounter = headerLineCounter + 1
end if

end sub


sub printWarning lineNumber, headerName$, warningNumber

if headerName$ = "" then
	print "warning on line " ; str$(lineNumber) ; " in parent file:  " ; warningCheckTable$(warningNumber, 2)
else
	print "warning on line " ; str$(lineNumber) ; " in file " ; headerName$ ; ":  " ; warningCheckTable$(warningNumber, 2)
end if


end sub


sub warningCheck byref warningCounter, warnListCount, lineNumber, headerName$, currentLine$, previousLine$, nextLine$

currentLine$ = lower$(trim$(currentLine$))

for i = 1 to warnListCount
	' now check for exceptions
	skipMeFlag = 0
	for j = 1 to exceptions(i)
		' first format the current line to the length of the warning fragment
		currentLineFragment$ = left$(currentLine$, len(warningCheckTable$(i, 1)) + len(warningCheckTable$(i, j + 4)))

		if currentLineFragment$ = warningCheckTable$(i, 1) + warningCheckTable$(i, j + 4) then
			skipMeFlag = 1
			exit for
		end if
	next

	if not(skipMeFlag) then
		currentLineFragment$ = left$(currentLine$, len(warningCheckTable$(i, 1)))
		if currentLineFragment$ = warningCheckTable$(i, 1) then
			select case
			case warningCheckTable$(i, 3) = "" and warningCheckTable$(i, 4) = ""
				' only look for the match
				warningCounter = warningCounter + 1
				call printWarning lineNumber, headerName$, i

			case warningCheckTable$(i, 3) = "" and warningCheckTable$(i, 4) <> ""
				' something must also match on the next line
				nextLineFragment$ = left$(lower$(trim$(nextLine$)), len(warningCheckTable$(i, 4)))

				if left$(warningCheckTable$(i, 4), 1) = "!" then
					' warn if argument is there
					if not(nextLineFragment$ = right$(warningCheckTable$(i, 4), len(warningCheckTable$(i, 4)) - 1)) then
						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if
				else
					if nextLineFragment$ = warningCheckTable$(i, 4) then
						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if
				end if

			case warningCheckTable$(i, 3) <> "" and warningCheckTable$(i, 4) = ""
				' something must also match on the previous line
				previousLineFragment$ = left$(lower$(trim$(previousLine$)), len(warningCheckTable$(i, 3)))

				if left$(warningCheckTable$(i, 3), 1) = "!" then
					' warn if argument is there
					if not(previousLineFragment$ = right$(warningCheckTable$(i, 3), len(warningCheckTable$(i, 3)) - 1)) then
						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if
				else
					if previousLineFragment$ = warningCheckTable$(i, 3) then
						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if
				end if

			case warningCheckTable$(i, 3) <> "" and warningCheckTable$(i, 4) <> ""
				' something must also match on the previous and next line
				previousLineFragment$ = left$(lower$(trim$(previousLine$)), len(warningCheckTable$(i, 3)))
				nextLineFragment$ = left$(lower$(trim$(nextLine$)), len(warningCheckTable$(i, 4)))

				select case
				case left$(warningCheckTable$(i, 3), 1) = "!" and left$(warningCheckTable$(i, 4), 1) = "!"
					doubleWarnCheckMode = 1
				case left$(warningCheckTable$(i, 3), 1) = "!" and left$(warningCheckTable$(i, 4), 1) <> "!"
					doubleWarnCheckMode = 2
				case left$(warningCheckTable$(i, 3), 1) <> "!" and left$(warningCheckTable$(i, 4), 1) = "!"
					doubleWarnCheckMode = 3
				case left$(warningCheckTable$(i, 3), 1) <> "!" and left$(warningCheckTable$(i, 4), 1) <> "!"
					doubleWarnCheckMode = 4
				end select

				select case doubleWarnCheckMode
				case 1
					' chop off not operators and warn if arguments is not there
					if not(previousLineFragment$ = _
					right$(warningCheckTable$(i, 3), len(warningCheckTable$(i, 3)) - 1) and _
					nextLineFragment$ = _
					right$(warningCheckTable$(i, 4), len(warningCheckTable$(i, 4)) - 1)) then

						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if

				case 2
					if not(previousLineFragment$ = _
					right$(warningCheckTable$(i, 3), len(warningCheckTable$(i, 3)) - 1)) and _
					nextLineFragment$ = _
					warningCheckTable$(i, 4) then

						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if

				case 3
					if previousLineFragment$ = _
					warningCheckTable$(i, 3) and _
					not(nextLineFragment$ = _
					right$(warningCheckTable$(i, 4), len(warningCheckTable$(i, 4)) - 1)) then

						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if

				case 4
					if previousLineFragment$ = _
					warningCheckTable$(i, 3) and _
					nextLineFragment$ = _
					warningCheckTable$(i, 4) then

						warningCounter = warningCounter + 1
						call printWarning lineNumber, headerName$, i
					end if

				end select
			end select
		end if
	end if
next

end sub


sub showHelp
	print ""
	print "syntax for this program:"
	print "aplombscribe -a -b -s -t [-d | -r] 'bodyfile.bas' 'c:\path_to_liberty_basic\liberty.exe'"
	print ""
	print "example to run program on 64 bit system:"
	print "aplombscribe -a -r 'myprogram.bas' 'c:\Program Files (x86)\Liberty BASIC v4.5.1\liberty.exe'"
	print ""
	print "Liberty Basic specific compiler switches:"
	print "-a : close all windows on exit"
	print "-d : debug"
	print "-r : run"
	print ""
	print "this program's switches:"
	print "-b : build mode, don't launch liberty basic just make DONOTEDIT file"
	print "-s : enable user defined strict warnings (stop on warning)"
	print "-t : turn on test mode (don't write changes to disk, don't launch anything)"
	print "-e : turn on expert mode"
	print "-v : print program version"
	print ""
	print "strict mode can be slow, using it with test mode recommended"
	print ""
	print "Dedicated to Aaron Swartz"
	print ""

end sub


sub showVersion ver$
	print "Program version v" ; ver$
end sub


sub timeSleep sleepTime
   open "kernel32" for DLL as #k
	  calldll #k, "Sleep", sleepTime as long, ret as void
   close #k
end sub

