@echo off

rem aplombscribe pre-Compiler batch file

if "%1" == "" goto case_help
if "%2" == "" goto case_secondargumenterror
rem check for basic file extension
if not "%~x1" == ".bas" goto case_firstargumenterror


aplombscribe -a %2 %3 %4 %5 %6 %7 %8 %9 '%1' 'c:\Program Files (x86)\Liberty BASIC v4.5.1\liberty.exe'
rem aplombscribe -a %2 %3 %4 %5 %6 %7 %8 %9 '%1' 'c:\Program Files (x86)\Liberty BASIC Pro v4.04\lbpro.exe'

goto end

:case_firstargumenterror
echo.
echo ERROR:  first argument must be a basic file name
:case_secondargumenterror
echo ERROR:  must have a second argument (switch)

:case_help
echo.
echo launches Aplomb Scribe (Liberty Basic Precompiler)
echo.
echo usage:
rem we need the caret (^) for escaping
echo pc filename.bas ^[-r ^| -d^] -s -t
echo.
echo -d : debug
echo -r : run
echo -s : enable user defined strict warnings (stop on warning)
echo -t : turn on test mode (don't write changes to disk, don't launch anything)
echo.
echo strict mode can be slow, using it with test mode recommended
echo.

:end
