' test single lines only - don't look at lines before or after.

gosub [haveFun]

UpperLeftX = 1
UpperLeftY = 1

textbox #noting.bumkissTxt, 2, 2, 250, 60
open "main window example" for window_nf as #noting
print #noting, "trapclose [closeNoting]"

print #noting.bumkissTxt, "We've got bumkiss"

goto [continue1]


[closeNoting]

close #noting

end


[haveFun]

print "in a gosub called 'haveFun'"

return


[continue1]
